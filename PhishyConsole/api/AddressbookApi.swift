//
//  AddressbookApi.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

class AddressbookApi {
    
    static func loadAddressbooks() {
        ApiController.getData(path: "addressbook") { data in
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let abs = try jsonDecoder.decode([String].self, from: data)
        
                
                DispatchQueue.main.async {
                    
                    let addressbooks = abs.map({Addressbook(name: $0)})
                    
                    Data.instance.addressbooks = addressbooks
                    
                }
                
                
            
            } catch let error {
                print(error)
            }
            
        }
    }
    
}
