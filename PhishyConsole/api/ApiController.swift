//
//  ApiController.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

class ApiController {
    
    static func getData(path: String, callback: @escaping (Foundation.Data) -> Void) {
        
        let profile = Data.instance.profile
        let session = URLSession.shared
        let url = URL(string: profile.host + "/phishy/api/" + path)!
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print(error)
                return
                
            }
            guard let data = data else { return }
            
            callback(data)
            
        }
        task.resume()
        
        
    }
    
    static func putData(path: String, name: String, data: Foundation.Data, callback: @escaping (Foundation.Data) -> Void) {
        
        let profile = Data.instance.profile
        let session = URLSession.shared
        let url = URL(string: profile.host + "/phishy/api/" + path + "/" + name)!
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.httpBody = data
        
        let task = session.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print(error)
                return
                
            }
            guard let data = data else { return }
            
            callback(data)
            
        }
        task.resume()
        
    }
    
    static func postData(path: String, name: String, callback: @escaping (Foundation.Data?) -> Void) {
        
        
        let profile = Data.instance.profile
        let session = URLSession.shared
        let url = URL(string: profile.host + "/phishy/api/" + path + "/" + name)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let task = session.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print(error)
                callback(nil)
                return
                
            }
            guard let data = data else { return }
            
            callback(data)
            
        }
        task.resume()
        
    }
    
    static func postData(path: String, callback: @escaping (Foundation.Data?) -> Void) {
        
        
        let profile = Data.instance.profile
        let session = URLSession.shared
        let url = URL(string: profile.host + "/phishy/api/" + path)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let task = session.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print(error)
                callback(nil)
                return
                
            }
            guard let data = data else { return }
            
            callback(data)
            
        }
        task.resume()
        
    }
    
    static func deleteData(path: String, name: String, callback: @escaping (Foundation.Data) -> Void) {
        
        
        let profile = Data.instance.profile
        let session = URLSession.shared
        let url = URL(string: profile.host + "/phishy/api/" + path + "/" + name)!
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        let task = session.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print(error)
                return
                
            }
            guard let data = data else { return }
            
            callback(data)
            
        }
        task.resume()
        
    }
    
}
