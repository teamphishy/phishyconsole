//
//  CampaignApi.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

class CampaignApi {
    
    static func loadCampaigns() {
        
        ApiController.getData(path: "campaign") { data in
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let campaigns = try jsonDecoder.decode([CampaignInfo].self, from: data)
               
                print(campaigns)
                
                DispatchQueue.main.async {
                    
                    
                    
                    Data.instance.campaigns = [Campaign]()
                    
                    for campaign in campaigns {
                        
                        Data.instance.campaigns.append(Campaign.empty(name: campaign.id))
                        
                    }
                }
                
                
            
            } catch let error {
                print(error)
            }
            
        }
        
    }
    
    static func loadCampaignDetail(id: String, callback: ((Campaign) -> Void)?) {
        
        ApiController.getData(path: "campaign/\(id)") { data in
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let campaign = try jsonDecoder.decode(CampaignDetail.self, from: data)
                
                print(campaign)
                
                DispatchQueue.main.async {
                    
                    let index = Data.instance.campaigns.firstIndex(where: {$0.name == campaign.id})!
                    var templates = [String: String]()
                    templates[TemplateTypes.infoMail.rawValue] = campaign.templates.infoMail
                    templates[TemplateTypes.infoWebsite.rawValue] = campaign.templates.infoWebsite
                    templates[TemplateTypes.phishMail.rawValue] = campaign.templates.phishMail
                    templates[TemplateTypes.phishWebsite.rawValue] = campaign.templates.phishWebsite
                    
                    Data.instance.objectWillChange.send()
                    var campaigns = Data.instance.campaigns
                    let campaign = Campaign(name: campaign.id, recipientSetSize: campaign.recipientSetSize, mailLifetime: campaign.mailLifetime, mailInterval: campaign.mailInterval, addressbook: campaign.addressbook, templates: templates, statistics: Data.instance.campaigns[index].statistics, active: campaign.status == "active")
                    campaigns[index] = campaign
                    Data.instance.campaigns = campaigns
                    
                    callback?(campaign)
                    
                }
                
                
            
            } catch {
                print(error)
            }
            
        }
        
    }
    
    static func updateCampaign(campaign: Campaign, callback: @escaping () -> Void) {
        
        print("Update campaign")
        
        let templateDetail = TemplateInfo(infoMail: campaign.templates["infoMail"] ?? "", infoWebsite: campaign.templates["infoWebsite"] ?? "", phishMail: campaign.templates["phishMail"] ?? "", phishWebsite: campaign.templates["phishWebsite"] ?? "")
        let campaignDetail = CampaignDetail(id: campaign.name, addressbook: campaign.addressbook, mailLifetime: campaign.mailLifetime, recipientSetSize: campaign.recipientSetSize, mailInterval: campaign.mailInterval, status: campaign.active ? "active" : "inactive", templates: templateDetail)
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(campaignDetail)
        
            ApiController.putData(path: "campaign", name: campaign.name, data: data) { data in
                
                if data == nil {
                    callback()
                    return
                }
                
                print("sent update")
                print(data)
                
                updateCampaignCallback(data: data, callback: callback)
                
            }
            
        } catch let error {
            print(error)
        }
        
    }
    
    static func sendCampaign(campaign: Campaign, callback: @escaping () -> Void) {
        
        print("send Campaign")
        
        do {
            ApiController.postData(path: "send", name: campaign.name) { data in
                print("campaign sent")
                
                callback()
            }
        } catch let error {
            print(error)
        }
        
    }
    
    static func evaluateCampaign(campaign: Campaign, callback: @escaping () -> Void) {
        
        print("evaluate Campaign")
        
        do {
            ApiController.postData(path: "evaluate", name: campaign.name) { data in
                print("camapign evaluated")
                
                callback()
            }
        } catch let error {
            print(error)
        }
        
    }
    
    static func updateCampaignCallback(data: Foundation.Data, callback: @escaping () -> Void) {
        
        let jsonDecoder = JSONDecoder()
        
        do {
            let campaign = try jsonDecoder.decode(CampaignDetail.self, from: data)
            
            print(campaign)
            
            DispatchQueue.main.async {
                
                let index = Data.instance.campaigns.firstIndex(where: {$0.name == campaign.id})!
                var templates = [String: String]()
                templates[TemplateTypes.infoMail.rawValue] = campaign.templates.infoMail
                templates[TemplateTypes.infoWebsite.rawValue] = campaign.templates.infoWebsite
                templates[TemplateTypes.phishMail.rawValue] = campaign.templates.phishMail
                templates[TemplateTypes.phishWebsite.rawValue] = campaign.templates.phishWebsite
                
                Data.instance.objectWillChange.send()
                var campaigns = Data.instance.campaigns
                let campaign = Campaign(name: campaign.id, recipientSetSize: campaign.recipientSetSize, mailLifetime: campaign.mailLifetime, mailInterval: campaign.mailInterval, addressbook: campaign.addressbook, templates: templates, statistics: Data.instance.campaigns[index].statistics, active: campaign.status == "active")
                campaigns[index] = campaign
                Data.instance.campaigns = campaigns
                
                
                
            }
            
            callback()
            
            
        
        } catch {
            print(error)
        }
        
    }
    
    static func createCampaign(name: String) {
        
        ApiController.postData(path: "campaign", name: name) { data in
            CampaignApi.loadCampaigns()
        }
        
    }
    
    static func deleteCampaign(name: String) {
        
        ApiController.deleteData(path: "campaign", name: name) { data in
            CampaignApi.loadCampaigns()
        }
        
    }
    
    
}
