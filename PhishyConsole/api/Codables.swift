//
//  Codables.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

struct CampaignInfo: Codable {
    let id: String
    let status: String
}

struct CampaignDetail: Codable {
    let id: String
    let addressbook: String
    let mailLifetime: Int
    let recipientSetSize: Int
    let mailInterval: Int
    let status: String
    let templates: TemplateInfo
    
}

struct TemplateInfo: Codable {
    let infoMail: String
    let infoWebsite: String
    let phishMail: String
    let phishWebsite: String
}

struct StatInfo: Codable {
    
    let numSentMails: Int
    let numPhishedMails: Int
    let numReportedMails: Int
    
}

struct TemplateListInfo: Codable {
    let groups: [GroupNameInfo]
    let templates: [TemplateNameInfo]
}

struct TemplateNameInfo: Codable {
    let id: String
}

struct GroupNameInfo: Codable {
    let id: String
}

struct AddressbookInfo: Codable {
    let id: String
}
