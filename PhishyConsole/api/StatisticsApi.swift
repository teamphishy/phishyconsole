//
//  StatisticsApi.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

class StatisticsApi {
    
    static func loadCumulatedStatistics() {
        
        ApiController.getData(path: "stat") { data in
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let stat = try jsonDecoder.decode(StatInfo.self, from: data)
        
                
                DispatchQueue.main.async {
                    print(stat)
                    Data.instance.cumulatedStatistics = Statistics(sentMails: stat.numSentMails, reportedMails: stat.numReportedMails, phishedMails: stat.numPhishedMails)
                    
                }
                
                
            
            } catch let error {
                print(error)
            }
            
        }
        
    }
    
    static func loadCampaignStatistics(name: String, callback: ((Statistics) -> Void)?) {
        
        ApiController.getData(path: "stat/\(name)") { data in
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let stat = try jsonDecoder.decode(StatInfo.self, from: data)
        
                
                DispatchQueue.main.async {
                    print(stat)
                    
                    let index = Data.instance.campaigns.firstIndex(where: {$0.name == name})!
                    Data.instance.objectWillChange.send()
                    let statics = Statistics(sentMails: stat.numSentMails, reportedMails: stat.numReportedMails, phishedMails: stat.numPhishedMails)
                    Data.instance.campaigns[index].statistics = statics
                    
                    callback?(statics)
                    
                }
                
                
            
            } catch let error {
                print(error)
            }
            
        }
        
    }
    
}
