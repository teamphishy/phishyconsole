//
//  TemplatesApi.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

class TemplatesApi {
    
    static func loadTemplatesInfo() {
        
        ApiController.getData(path: "template") { data in
            
            let jsonDecoder = JSONDecoder()
            
            do {
                let tlist = try jsonDecoder.decode(TemplateListInfo.self, from: data)
        
                
                DispatchQueue.main.async {
                    print(tlist)
                    
                    var groups = tlist.groups.map({TemplateGroup(name: $0.id, templates: [String]())})
                    tlist.templates.forEach { template in
                        let groupIndex = groups.firstIndex(where: {
                            let length = $0.name.count
                            let prefix = template.id.prefix(length + 1)
                            return $0.name + "-" == prefix
                            
                        })
                        if let idx = groupIndex {
                            groups[idx].templates.append(template.id)
                        }
                    }
                    
                    Data.instance.objectWillChange.send()
                    Data.instance.templateGroups = groups
                    
                }
                
                
            
            } catch let error {
                print(error)
            }
            
        }
        
    }
    
    static func createTemplateGroup(name: String) {
        
        ApiController.postData(path: "template", name: name) { data in
            
            loadTemplatesInfo()
            
        }
        
    }
    
    static func deleteTemplateGroup(name: String) {
        
        ApiController.deleteData(path: "template", name: name) { data in
            loadTemplatesInfo()
        }
    }
    
}
