//
//  Addressbook.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

struct Addressbook {
    
    var name: String
    
    static func empty(name: String) -> Addressbook {
        return Addressbook(name: name)
    }
    
}
