//
//  Campaign.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

struct Campaign {
    
    var name: String
    var recipientSetSize: Int
    var mailLifetime: Int
    var mailInterval: Int
    var addressbook: String
    var templates: [String: String]
    var statistics: Statistics
    var active: Bool
    
    static func empty(name: String) -> Campaign {
        var templates = [String: String]()
        templates[TemplateTypes.phishMail.rawValue] = ""
        templates[TemplateTypes.infoMail.rawValue] = ""
        templates[TemplateTypes.phishWebsite.rawValue] = ""
        templates[TemplateTypes.infoWebsite.rawValue] = ""
        return Campaign(name: name, recipientSetSize: 0, mailLifetime: 0, mailInterval: 0, addressbook: "", templates: templates, statistics: Statistics.empty(), active: false)
    }
    
    func clone() -> Campaign {
        
        return Campaign(name: name, recipientSetSize: recipientSetSize, mailLifetime: mailLifetime, mailInterval: mailInterval, addressbook: addressbook, templates: templates, statistics: statistics, active: active)
        
    }
    
    mutating func update(update: Campaign) {
        
        name = update.name
        recipientSetSize = update.recipientSetSize
        mailLifetime = update.mailLifetime
        mailInterval = update.mailInterval
        addressbook = update.addressbook
        templates = update.templates
        statistics = update.statistics
        
    }
    
}
