//
//  Data.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation
import Combine

final class Data: ObservableObject {
    
    @Published var campaigns: [Campaign]
    @Published var templateGroups: [TemplateGroup]
    @Published var profile: Profile
    @Published var profiles: [Profile]
    @Published var cumulatedStatistics: Statistics
    @Published var addressbooks: [Addressbook]
    
    public static var instance = Data()
    
    public init() {
        
        campaigns = [Campaign]()
        templateGroups = [TemplateGroup]()
        addressbooks = [Addressbook]()
        profile = Profile.default
        profiles = Profile.getProfiles()
        cumulatedStatistics = Statistics.empty()
        
        
        /*campaigns.append(Campaign.empty(name: "Test"))
        campaigns.append(Campaign.empty(name: "Second"))
        
        var complexTemplates = [String: String]()
        complexTemplates["phishMail"] = "xd"
        var complex = Campaign(name: "xd", recipientSetSize: 300, mailLifetime: 14, mailInterval: 60, addressbook: "ldap", templates: complexTemplates, statistics: Statistics(sentMails: 200, reportedMails: 50, phishedMails: 90), active: true)
        campaigns.append(complex)
        
        
        templateGroups.append(TemplateGroup(name: "First", templates: ["First-mail", "First-website"]))
        templateGroups.append(TemplateGroup(name: "Second", templates: ["Second-mail", "Second-website"]))
        
        addressbooks.append(Addressbook(name: "book"))*/
        
    }
    
    public func getCampaign(name: String) -> Campaign {
        
        return campaigns.filter({$0.name == name})[0]
        
    }
    
}
