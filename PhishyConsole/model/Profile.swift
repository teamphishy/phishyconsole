//
//  Profile.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

struct Profile : Identifiable {
    
    var host: String
    var user: String
    var password: String
    
    var id: String {
        return "\(user)@\(host)"
    }
     
    
    static func empty() -> Profile {
        return Profile(host: "", user: "", password: "")
    }
    
    static var `default`: Profile {
        
        let userDefaults = UserDefaults.standard
        let hosts = userDefaults.stringArray(forKey: "hosts") ?? ["http://localhost"]
        let users = userDefaults.stringArray(forKey: "users") ?? [""]
        let currentProfile = userDefaults.integer(forKey: "profile")
        
        return Profile(host: hosts[currentProfile], user: users[currentProfile], password: "")
        
    }
    
    static func persist(profiles: [Profile]) {
        
        let hosts = profiles.map({$0.host})
        let users = profiles.map({$0.user})
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(hosts, forKey: "hosts")
        userDefaults.set(users, forKey: "users")
        
        if (userDefaults.integer(forKey: "profile") >= profiles.count) {
            userDefaults.set(0, forKey: "profile")
        }

    }
    
    static func persistProfileSwitch(profile: Int) {
        
        let userDefaults = UserDefaults.standard
        
        userDefaults.set(profile, forKey: "profile")
        
    }
    
    static func countProfiles() -> Int {
        
        let userDefaults = UserDefaults.standard
        
        return userDefaults.stringArray(forKey: "hosts")?.count ?? 0
        
    }
    
    static func getProfiles() -> [Profile] {
        
        var profiles = [Profile]()
        
        let userDefaults = UserDefaults.standard
        let hosts = userDefaults.stringArray(forKey: "hosts") ?? ["http://localhost"]
        let users = userDefaults.stringArray(forKey: "users") ?? [""]
        
        for i in 0..<hosts.count {
            
            profiles.append(Profile(host: hosts[i], user: users[i], password: ""))
            
        }
        
        return profiles
        
    }
    
}
