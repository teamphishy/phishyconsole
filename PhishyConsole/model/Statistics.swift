//
//  Statistics.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

struct Statistics {
    
    var sentMails: Int
    var reportedMails: Int
    var phishedMails: Int
    
    static func empty() -> Statistics {
        return Statistics(sentMails: 0, reportedMails: 0, phishedMails: 0)
    }
    
}
