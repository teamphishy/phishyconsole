//
//  Template.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

enum TemplateTypes: String, CaseIterable {
    case phishMail = "phishMail"
    case infoMail = "infoMail"
    case phishWebsite = "phishWebsite"
    case infoWebsite = "infoWebsite"
}
