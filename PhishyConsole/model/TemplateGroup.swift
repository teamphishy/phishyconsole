//
//  TemplateGroup.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import Foundation

struct TemplateGroup {
    
    var name: String
    var templates: [String]
    
}
