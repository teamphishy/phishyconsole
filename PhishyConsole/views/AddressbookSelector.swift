//
//  AddressbookSelector.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct AddressbookSelector: View {
    
    @EnvironmentObject var data: Data
    @Binding var isVisible: Bool
    @Binding var newAddressbookName: String
    
    
    var body: some View {
        
        VStack {
        
            HStack {
                Button("Cancel") {
                    self.newAddressbookName = ""
                    self.isVisible = false
                }
                
                Spacer()
            }.padding()
            
            List {
                
                if data.addressbooks.count == 0 {
                    Text("There are no Addressbooks known to the Server")
                    .foregroundColor(.gray)
                } else {
                
                    ForEach(data.addressbooks, id: \.name) { addressbook in
                        
                        Button(addressbook.name) {
                            
                            self.newAddressbookName = addressbook.name
                            self.isVisible = false
                            
                        }
                        
                    }
                }
                
            }
        }.onAppear() {
            AddressbookApi.loadAddressbooks()
        }
        
    }
}

struct AddressbookSelector_Previews: PreviewProvider {
    static var previews: some View {
        AddressbookSelector(isVisible: .constant(false), newAddressbookName: .constant(""))
        .environmentObject(Data())
    }
}
