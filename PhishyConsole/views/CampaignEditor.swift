//
//  CampaignEditor.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct CampaignEditor: View {
    
    @EnvironmentObject var data: Data
    @Binding var campaign: Campaign
    @State var deleteAltertVisible = false
    @State var addTemplateVisible = false
    @State var newTemplateName = ""
    @State var newTemplateType = ""
    @State var selectAddressbookVisible = false
    @State var newAddressbookName = ""
    
    var recipientSetSizeProxy: Binding<String> {
        Binding<String>(
            get:{ String(format: "%d", self.campaign.recipientSetSize)},
            set:{
                if let value = NumberFormatter().number(from: $0) {
                    self.campaign.recipientSetSize = value.intValue
                }
            }
        )
    }
    
    var mailLifetimeProxy: Binding<String> {
        Binding<String>(
            get:{ String(format: "%d", self.campaign.mailLifetime)},
            set:{
                if let value = NumberFormatter().number(from: $0) {
                    self.campaign.mailLifetime = value.intValue
                }
            }
        )
    }
    
    var mailIntervalProxy: Binding<String> {
        Binding<String>(
            get:{ String(format: "%d", self.campaign.mailInterval)},
            set:{
                if let value = NumberFormatter().number(from: $0) {
                    self.campaign.mailInterval = value.intValue
                }
            }
        )
    }
    
    var body: some View {
        
        List {
            HStack {
                Toggle(isOn: $campaign.active) {
                    Text("Active")
                }

            }
            HStack {
                
                Button(action: {
                    self.selectAddressbookVisible = true
                }) {
                    
                    HStack {
                        Text("Addressbook")
                            .frame(width: 150, alignment: .leading)
                        Text(self.campaign.addressbook == "" ? "..." : self.campaign.addressbook)
                            .frame(alignment: .trailing)
                            .foregroundColor(.blue)
                    }
                    
                }
                .buttonStyle(PlainButtonStyle())
                .sheet(isPresented: self.$selectAddressbookVisible) {
                    AddressbookSelector(isVisible: self.$selectAddressbookVisible, newAddressbookName: self.$newAddressbookName)
                        .environmentObject(self.data)
                    .onAppear() {
                        self.newAddressbookName = ""
                    }
                    .onDisappear() {
                        if self.newAddressbookName != "" {
                            
                            var c = self.campaign
                            c.addressbook = self.newAddressbookName
                            self.campaign = c
                            
                        }
                    }
                }
            }
            HStack {
                Text("Mail Lifetime")
                    .frame(width: 150, alignment: .leading)
                Spacer()
                TextField("Mail Lifetime", text: self.mailLifetimeProxy)
                    .keyboardType(.numberPad)
            }
            HStack {
                Text("Mail Interval")
                .frame(width: 150, alignment: .leading)
                Spacer()
                TextField("Mail Interval", text: self.mailIntervalProxy)
                .keyboardType(.numberPad)
            }
            HStack {
                Text("Recipient Set Size")
                .frame(width: 150, alignment: .leading)
                Spacer()
                TextField("Recipient Set Size", text: self.recipientSetSizeProxy)
                .keyboardType(.numberPad)
            }
            Section(header: Text("Templates")) {                
                ForEach(campaign.templates.sorted(by: >), id: \.key) { key, value in
                    
                    Button(action: {
                        self.newTemplateType = key
                        self.addTemplateVisible = true
                    }) {
                        HStack {
                            Text(key)
                            .frame(width: 150, alignment: .leading)
                            
                            Text(value == "" ? "..." : value)
                                .foregroundColor(.blue)
                                .frame(alignment: .trailing)
                        }
                    }.buttonStyle(PlainButtonStyle())
                    
                }
                
                
            }
            
            
            
            Section(header: Text("Management")) {
                Button("Delete") {
                    self.deleteAltertVisible = true
                }
                .foregroundColor(.red)
                .alert(isPresented: $deleteAltertVisible) {
                    Alert(title: Text("Delete \(campaign.name)?"), message: Text("Deleting \(campaign.name) will remove all settings and statistics for campaign \(campaign.name). You cannot undo this action"), primaryButton: .cancel(), secondaryButton: .destructive(Text("Delete"), action: {
                        
                            CampaignApi.deleteCampaign(name: self.campaign.name)
                        
                        }
                        ))
                    
                }
            }
        }
        .listStyle(GroupedListStyle())
        .navigationBarTitle(Text(campaign.name))
        .sheet(isPresented: self.$addTemplateVisible) {
            TemplateSelector(isVisible: self.$addTemplateVisible, newTemplateName: self.$newTemplateName)
                .environmentObject(self.data)
            .onAppear() {
                self.newTemplateName = ""
            }
            .onDisappear() {
                if self.newTemplateName != "" {
                    print(self.newTemplateName)
                    
                    var c = self.campaign
                    c.templates[self.newTemplateType] = self.newTemplateName
                    self.campaign = c
                    
                    print("new Template \(self.newTemplateName) \(self.newTemplateType)")
                    
                }
            }
        }
        
        
    }
}

struct CampaignEditor_Previews: PreviewProvider {
    static var previews: some View {
        CampaignEditor(campaign: .constant(Campaign.empty(name: "Test")))
        .environmentObject(Data())
    }
}
