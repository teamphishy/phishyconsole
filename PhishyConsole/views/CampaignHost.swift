//
//  CampaignHost.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct CampaignHost: View {
    
    @EnvironmentObject var data: Data
    @State var campaign: Campaign = Campaign.empty(name: "")
    @State var editMode = false
    var campaignId: String
    
    var body: some View {
        
        VStack {
            if !editMode {
            
                CampaignOverview(campaign: $campaign)
                .environmentObject(data)
                .onAppear() {
                    self.campaign = self.data.getCampaign(name: self.campaignId)
                }
                .navigationBarItems(trailing: Button("Edit") {
                    self.editMode = true
                })
                
                
            } else {
                
                CampaignEditor(campaign: $campaign)
                .environmentObject(data)
                .onAppear() {
                    self.campaign = self.data.getCampaign(name: self.campaignId)
                }
                .navigationBarItems(trailing: Button("Save") {
                    
                    self.data.objectWillChange.send()
                    let index = self.data.campaigns.firstIndex(where: {$0.name == self.campaignId})!
                    self.data.campaigns[index] = self.campaign
                    
                    CampaignApi.updateCampaign(campaign: self.campaign) {
                        
                        self.editMode = false
                        
                    }
                    
                })
                
            }
        }
        
    }
}

struct CampaignHost_Previews: PreviewProvider {
    static var previews: some View {
        CampaignHost(campaignId: "xd")
        .environmentObject(Data())
    }
}
