//
//  CampaignList.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct CampaignList: View {
    
    @EnvironmentObject var data: Data
    @State var showingSettings = false
    @State var showingNewCampaign = false
    
    var body: some View {
        NavigationView {
            
            VStack(alignment: .leading) {
                ScrollView(.horizontal, showsIndicators: false) {
                    StatisticsView(statistics: data.cumulatedStatistics)
                    .padding()
                }
                
                
                
                List {
                    
                    if data.campaigns.count == 0 {
                        Text("There are no Campaigns yet.\nYou can start by creating a new Campaign")
                        .foregroundColor(.gray)
                    } else {
                    
                    
                        ForEach(data.campaigns, id: \.name) { campaign in
                            NavigationLink(
                            destination: CampaignHost(campaignId: campaign.name)
                                ) {
                                Text(campaign.name)
                            }
                        }
                        
                    }
                    
                    Button("New Campaign...") {
                        
                        self.showingNewCampaign = true
                        
                    }
                    .frame(alignment: .leading)
                    .foregroundColor(.blue)
                    
                    
                }.alert(isPresented: self.$showingNewCampaign, TextAlert(title: "Campaign Name") {
                    if let name = $0 {
                        CampaignApi.createCampaign(name: name)
                    }
                })
                
                Spacer()
                
                Text(data.profile.id)
                    .font(.caption)
                    .foregroundColor(.gray)
                    .padding()
            }
            .navigationBarTitle("Campaigns")

                .navigationBarItems(trailing: NavigationLink(destination: SettingsHost()
                    .environmentObject(self.data)) {
                        Image(systemName: "person.crop.circle")
                         .imageScale(.large)
                })
            //.listStyle(SidebarListStyle())
        }.onAppear() {
            CampaignApi.loadCampaigns()
            StatisticsApi.loadCumulatedStatistics()
        }
    }
}

struct CampaignList_Previews: PreviewProvider {
    static var previews: some View {
        CampaignList()
        .environmentObject(Data())
    }
}
