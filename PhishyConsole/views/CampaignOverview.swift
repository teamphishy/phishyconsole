//
//  CampaignOverview.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct CampaignOverview: View {

    @Binding var campaign: Campaign
    @State var mailLifetime = "0"
    @State var showAddTemplate = false
    
    var body: some View {
        
        VStack(alignment: .center) {
            
            ScrollView(.horizontal, showsIndicators: false) {
                StatisticsView(statistics: campaign.statistics)
                    .padding()
            }
            
            List {
                HStack {
                    Toggle(isOn: $campaign.active) {
                        Text("Active")
                    }.disabled(true)

                }
                HStack {
                    Text("Addressbook")
                        .frame(width: 150, alignment: .leading)
                    Spacer()
                    Text(campaign.addressbook)
                }
                HStack {
                    Text("Mail Lifetime")
                        .frame(width: 150, alignment: .leading)
                    Spacer()
                    Text("\(campaign.mailLifetime)")
                }
                HStack {
                    Text("Mail Interval")
                    .frame(width: 150, alignment: .leading)
                    Spacer()
                    Text("\(campaign.mailInterval)")
                }
                HStack {
                    Text("Recipient Set Size")
                    .frame(width: 150, alignment: .leading)
                    Spacer()
                    Text("\(campaign.recipientSetSize)")
                }
                //Text("Templates")
                 //   .font(.headline)
                Section(header: Text("Templates")) {
                    ForEach(campaign.templates.sorted(by: >), id: \.key) { key, value in
                        
                        HStack {
                            Text(key)
                                .frame(width: 150, alignment: .leading)
                            Text(value)
                        }
                    }
                }
                
                Section(header: Text("Manual Action")) {
                    Button("Send Phish Mails") {
                        CampaignApi.sendCampaign(campaign: campaign) {
                            Alert(title: Text("Campaign sent"))
                        }
                    }.foregroundColor(.blue)
                    Button("Update Reported Mails") {
                        CampaignApi.evaluateCampaign(campaign: campaign) {
                            Alert(title: Text("Campaign will update reported mails"))
                        }
                    }.foregroundColor(.blue)
                    Button("Update Addressbook") {
                        ApiController.postData(path: "updateab") { _ in
                            Alert(title: Text("Addressbook Updated"))
                        }
                    }.foregroundColor(.blue)
                }
            }
        //.listStyle(GroupedListStyle())
        
        }
        .navigationBarTitle(campaign.name)
        .navigationBarItems(trailing: Button(action:{}) {
            Text("Edit")
        })
        .onAppear() {
            CampaignApi.loadCampaignDetail(id: self.campaign.name) {campaign in
                let statistics = self.campaign.statistics
                self.campaign = campaign
                self.campaign.statistics = statistics
                
                StatisticsApi.loadCampaignStatistics(name: self.campaign.name) { statistics in
                    self.campaign.statistics = statistics
                }
            }
            
        }
    }
}

struct CampaignOverview_Previews: PreviewProvider {
    static var previews: some View {
        CampaignOverview(campaign: .constant(Campaign.empty(name: "xd")))
            .environmentObject(Data())
    }
}
