//
//  ProfileSwitcher.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct ProfileSwitcher: View {
    
    @EnvironmentObject var data: Data
    @State var editingProfile: Profile = Profile.empty()
    
    var body: some View {
        
        List {
            ForEach(data.profiles, id: \.id) {profile in
                
                HStack {
                    
                    Button(action:{}) {
                    
                        VStack {
                            Text(profile.host)
                            Text(profile.user)
                                .font(.subheadline)
                            
                            if profile.id == self.data.profile.id {
                                Text("Current Profile")
                                    .font(.subheadline)
                                    .foregroundColor(.blue)
                            }
                        }
                        
                        Spacer()
                        
                        NavigationLink(destination: ProfileEditor(profile: self.$editingProfile)
                            .environmentObject(self.data)) {
                            Image(systemName: "info.circle")
                                .imageScale(.large)
                        }
                        
                    }.buttonStyle(PlainButtonStyle())
                    
                }
                
            }
        }
        
    }
}

struct ProfileSwitcher_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSwitcher()
        .environmentObject(Data())
    }
}
