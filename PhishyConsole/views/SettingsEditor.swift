//
//  SettingsEditor.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct SettingsEditor: View {
    
    @Binding var profile: Profile
    
    var body: some View {
        List {
            HStack {
                Text("Host").bold()
                    .frame(width: 100, alignment: .trailing)
                Divider()
                TextField("Host", text: $profile.host)
            }
            HStack {
                Text("User").bold()
                    .frame(width: 100, alignment: .trailing)
                Divider()
                TextField("User", text: $profile.user)
            }
            HStack {
                Text("Password").bold()
                    .frame(width: 100, alignment: .trailing)
                Divider()
                TextField("Password", text: $profile.password)
            }
        }.navigationBarTitle("Server")
    }
}

struct SettingsEditor_Previews: PreviewProvider {
    static var previews: some View {
        SettingsEditor(profile: .constant(Profile.empty()))
    }
}
