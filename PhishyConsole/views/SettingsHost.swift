//
//  SettingsHost.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct SettingsHost: View {
    
    @EnvironmentObject var data: Data
    @State var draftProfile = Profile.empty()
    @State var editMode = false
    
    var body: some View {

        
            VStack {
            
                
                if !editMode {
                    
                    SettingsSummary(profile: self.draftProfile)
                    .environmentObject(data)
                    .onAppear() {
                        self.draftProfile = self.data.profile
                    }
                    .navigationBarItems(trailing: Button("Edit") {
                        self.editMode = true
                    })
                    
                } else {
                    
                    
                        
                        SettingsEditor(profile: $draftProfile)
                        .environmentObject(data)
                        .onAppear() {
                            self.draftProfile = self.data.profile
                        }
                        .navigationBarItems(trailing: Button("Done") {
                            self.editMode = false
                            self.data.profile = self.draftProfile
                            Profile.persist(profiles: [self.data.profile])
                        })
                
                }
        }.navigationBarBackButtonHidden(editMode)

    }
}

struct SettingsHost_Previews: PreviewProvider {
    static var previews: some View {
        SettingsHost()
        .environmentObject(Data())
    }
}
