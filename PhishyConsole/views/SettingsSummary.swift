//
//  SettingsSummary.swift
//  PhishyConsole
//
//  Created by Daniel Ritz on 26.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct SettingsSummary: View {
    
    var profile: Profile
    
    var body: some View {
        
        List {
            HStack {
                Text("Host")
                    .frame(width: 100, alignment: .leading)
                Text(profile.host)
            }
            HStack {
                Text("Username")
                .frame(width: 100, alignment: .leading)
                Text(profile.user)
            }
            HStack {
                Text("Password")
                .frame(width: 100, alignment: .leading)
                Text("***")
            }
        }.navigationBarTitle("Server")
        
    }
}

struct SettingsSummary_Previews: PreviewProvider {
    static var previews: some View {
        SettingsSummary(profile: Profile.empty())
    }
}
