//
//  ContentView.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 15.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct StatisticsView: View {
    
    var statistics: Statistics
    
    var body: some View {
        HStack(spacing: 6.0) {
            VStack {
                Text("\(statistics.sentMails)")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.all, 10)
                    .foregroundColor(Color.blue)
                    
                Text("Mails sent")
                    .padding([.leading, .bottom, .trailing], 10)
            }
            .frame(width: 110.0, height: 80.0)
            .background(Color.blue.opacity(0.2))
            .cornerRadius(6)
            
            
            
            
                
            VStack {
                Text("\(statistics.phishedMails)")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.all, 10)
                    .foregroundColor(Color.red)
                Text("Mails phished")
                    .padding([.leading, .bottom, .trailing], 10)
            }
            .frame(width: 110.0, height: 80.0)
            .background(Color.red.opacity(0.2))
            .cornerRadius(6)
            
            VStack {
                Text("\(statistics.reportedMails)")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.all, 10)
                    .foregroundColor(Color.green)
                Text("Mails reported")
                    .padding([.leading, .bottom, .trailing], 10)
            }
            .frame(width: 110.0, height: 80.0)
            .background(Color.green.opacity(0.2))
            .cornerRadius(6)
        }
        //.frame(width: 400.0)
    }
}


struct StatisticsView_Previews: PreviewProvider {
    static var previews: some View {
        StatisticsView(statistics: Statistics(sentMails: 100, reportedMails: 50, phishedMails: 30))
    }
}
