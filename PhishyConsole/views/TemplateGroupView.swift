//
//  TemplateGroup.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct TemplateGroupView: View {
    
    var templateGroup: TemplateGroup
    
    var body: some View {
        VStack {
            
            Text(templateGroup.name)
                .font(.headline)
            
            ForEach(templateGroup.templates, id:\.self) { template in
                
                Text(template)
                
            }
            
        }
    }
}

struct TemplateGroupView_Previews: PreviewProvider {
    static var previews: some View {
        TemplateGroupView(templateGroup: TemplateGroup(name: "Test", templates: ["Test-xd", "Text-ad"]))
    }
}
