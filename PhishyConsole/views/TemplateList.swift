//
//  TemplateList.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct TemplateList: View {

    @EnvironmentObject var data: Data
    @Binding var selectorVisible: Bool
    @Binding var newTemplateName: String
    @State var newGroupVisible = false
    
    
    var body: some View {
        
        List {
            
            if (data.templateGroups.count == 0) {
            
                Text("There are no Templates available yet.\nBut you can start by creating a new Template Group")
                    .foregroundColor(.gray)
                
            } else {
            
                ForEach(data.templateGroups, id: \.name) { templateGroup in
                    
                    Section(header: TemplateGroupHeader(templateGroup: templateGroup)) {
                        ForEach(templateGroup.templates, id: \.self) {
                            template in
                            
                            Button(template) {
                                self.newTemplateName = template
                                self.selectorVisible = false
                            }.buttonStyle(PlainButtonStyle())
                            
                            
                        }
                    }
                    
                    
                }
            }
            
            Button("New Template Group...") {
                
                self.newGroupVisible = true
                
            }.alert(isPresented: self.$newGroupVisible, TextAlert(title: "Group Name") {
                
                if let name = $0 {
                    TemplatesApi.createTemplateGroup(name: name)
                }
                
            })
            
            
        }
        .listStyle(GroupedListStyle())
        .onAppear() {
            TemplatesApi.loadTemplatesInfo()
        }
        
        
    }
}

struct TemplateGroupHeader: View {
    
    var templateGroup: TemplateGroup
    @State var deleteAlertVisible = false
    
    var body: some View {
        
        HStack {
            Text(templateGroup.name)
            Spacer()
            Button(action: {
                self.deleteAlertVisible = true
            }) {
                Text("Delete")
            }.alert(isPresented: $deleteAlertVisible) {
                Alert(title: Text("Delete \(templateGroup.name)?"), message: Text("Deleting \(templateGroup.name) will remove all data for all templates in \(templateGroup.name). You cannot undo this action"), primaryButton: .cancel(), secondaryButton: .destructive(Text("Delete"), action: {
                    
                        TemplatesApi.deleteTemplateGroup(name: self.templateGroup.name)
                    
                    }
                    ))
                
            }
        }
        
    }
    
}

struct TemplateList_Previews: PreviewProvider {
    static var previews: some View {
        TemplateList(selectorVisible: .constant(false), newTemplateName: .constant(""))
        .environmentObject(Data())
    }
}
