//
//  TemplateSelector.swift
//  PhishyManager
//
//  Created by Daniel Ritz on 25.07.20.
//  Copyright © 2020 Daniel Ritz. All rights reserved.
//

import SwiftUI

struct TemplateSelector: View {
    
    @Binding var isVisible: Bool
    @Binding var newTemplateName: String
    
    var body: some View {
        VStack {
            
            HStack {
                Button(action: {self.isVisible = false}) {
                    Text("Cancel")
                }
                Spacer()
                
            }.padding()
            
            TemplateList(selectorVisible: self.$isVisible, newTemplateName: self.$newTemplateName)
            
            Spacer()
            
            
        }
    }
}

struct TemplateSelector_Previews: PreviewProvider {
    static var previews: some View {
        TemplateSelector(isVisible: .constant(true), newTemplateName: .constant(""))
        .environmentObject(Data())
    }
}
